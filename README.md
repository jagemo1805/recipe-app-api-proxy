# recipe api proxy 

nginx proxy app for our recipe app API

## usage

### environment variables

* `LISTEN_PORT` - ort to listen on (default: `8000`)
* `APP_HOST` - host name for the app to forward requests to (default: `app`)
* `APP_PORT` - port to forward requests to (default: `9000`)
